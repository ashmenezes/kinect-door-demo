﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeanManager : MonoBehaviour {    

    public KinectManager kinman;
    public GameObject head;
    public GameObject camera;
    public bool isCalibrated = true;
    public float lerp;

    Vector3 ogOrientPos;
    Quaternion ogOrientRot;
    Vector3 posDiff = Vector3.zero;
    Quaternion rotDiff = Quaternion.identity;
    Vector3 ogHeadPos;

    float min = 0f;
    float max = 0f;

    // Use this for initialization
    void Start () {
        kinman = camera.GetComponent<KinectManager>();
        ogOrientPos = camera.transform.position;
        ogOrientRot = camera.transform.rotation;
        head = GameObject.Find("03_Head");
        ogHeadPos = head.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        if (kinman.Player1Calibrated)
            isCalibrated = true;
        else
            isCalibrated = false;
        if (isCalibrated && ogOrientRot.eulerAngles != head.transform.rotation.eulerAngles)// && ogOrientPos.x != head.transform.position.x)
        {
            // convert head position to camera rotation
            // xpos: 5.5 to 7.5
            // yrot: 1 to 121 (331 to 91)
            // ypos: -0.5 to 0.8
            // xrot: 1 to 28 (353 to 20)

            float calcRoty = (head.transform.position.x * -25.222f) + 146.22f;

            // show angle to rotate
            /*float dist = camera.transform.eulerAngles.y - head.transform.eulerAngles.y;
            if (min == 0f)
                min = dist;
            if (max == 0f)
                max = dist;
            if (dist < min)
                min = dist;
            if (dist > max)
                max = dist;
            GameObject.Find("Text").GetComponent<Text>().text = "max: " + max;
            GameObject.Find("Min").GetComponent<Text>().text = "min: " + min;*/

            // calculate yrot based on xpos
            calcRoty = 360 - calcRoty;
            float LerpSmoothing = lerp * Time.deltaTime;
            //GameObject.Find("Lerp").GetComponent<Text>().text = "lerp: " + LerpSmoothing;

            // calculate fov based on zpos
            // zpos: -0.5 to 0.5
            // dep:    85 to 95
            //GameObject.Find("Text").GetComponent<Text>().text = "zpos: " + head.transform.position.z;
            //GameObject.Find("Min").GetComponent<Text>().text = "fov: " + camera.GetComponent<Camera>().fieldOfView;
            float calcFovZ = (head.transform.position.z * -10f) + 90f;
            camera.GetComponent<Camera>().fieldOfView = calcFovZ;

            // calculate xrot based on ypos
            //float calcRotx = (head.transform.position.x * 20.769f) - 11.385f;
            //calcRotx = 360 - calcRotx;
            //GameObject.Find("Text").GetComponent<Text>().text = "RotAngle: " + calcRotx;    //   1 to 51       (331 to 21)

            // apply changes to camera
            Vector3 eulerAngles = new Vector3(camera.transform.rotation.eulerAngles.x, -calcRoty, camera.transform.rotation.eulerAngles.z);
            camera.transform.rotation = Quaternion.Lerp(camera.transform.rotation, Quaternion.Euler(eulerAngles), LerpSmoothing);
            

        }
        else
        {
            //camera.transform.rotation = ogOrientRot;
            //camera.transform.position = ogOrientPos;
        }
    }
}
