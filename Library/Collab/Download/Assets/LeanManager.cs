﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeanManager : MonoBehaviour {

    public KinectManager kinman;
    public Vector3 ogOrientPos;
    public Quaternion ogOrientRot;
    public GameObject head;
    public GameObject camera;
    public bool samePos = true;
    public bool sameRot = true;

    Vector3 posDiff = Vector3.zero;
    Quaternion rotDiff = Quaternion.identity;

	// Use this for initialization
	void Start () {
        camera = GameObject.Find("Main Camera");
        kinman = camera.GetComponent<KinectManager>();
        ogOrientPos = camera.transform.position;
        ogOrientRot = camera.transform.rotation;
        head = GameObject.Find("03_Head");
    }
	
	// Update is called once per frame
	void Update () {
        if (kinman.Player1Calibrated && ogOrientPos != head.transform.position)// && ogOrientRot.eulerAngles != head.transform.rotation.eulerAngles)
        {
            camera.transform.position = head.transform.position;
            //camera.transform.rotation = head.transform.rotation;
        }
    }
}
